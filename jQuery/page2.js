$(document).ready(function() {

    $('#menuButton').click(function() {
        $('#page2Menu').show();
        $('#CIC').show();
        $('#DAC5').show();
        $('#DAC6').show();
        $('#BSC').show();
        $('#extraHelp').show();
    });

    $('#closeButton').click(function() {
        $('#page2Menu').hide();
    });

    $('#closeButton2, #closeButton3, #closeButton4, #closeButton5, #closeButton6').click(function() {
        $('#page2Menu').hide();
        $('#CICscroll').hide();
        $('#DAC5scroll').hide();
        $('#DAC6scroll').hide();
        $('#BSCscroll').hide();
        $('#extscroll').hide();
        $('#CIC1Page').hide();
        $('#CIC2Page').hide();
        $('#CIC3Page').hide();
        $('#CIC4Page').hide();
        $('#CIC5Page').hide();
        $('#CIC6Page').hide();
        $('#CIC7Page').hide();
        $('#CIC8Page').hide();
    });

    $('#backButton1, #backButton2, #backButton3, #backButton4, #backButton5').click(function() {
        $('#CICscroll').hide();
        $('#DAC5scroll').hide();
        $('#DAC6scroll').hide();
        $('#BSCscroll').hide();
        $('#extscroll').hide();
        $('#page2Menu').show();
        $('#CIC').show();
        $('#DAC5').show();
        $('#DAC6').show();
        $('#BSC').show();
        $('#extraHelp').show();                    
    });

    $('#backButton11, #backButton12, #backButton13, #backButton14, #backButton15, #backButton16, #backButton17, #backButton18').click(function() {
        $('#CIC1Page').hide();
        $('#CIC2Page').hide();
        $('#CIC3Page').hide();
        $('#CIC4Page').hide();
        $('#CIC5Page').hide();
        $('#CIC6Page').hide();
        $('#CIC7Page').hide();
        $('#CIC8Page').hide();
    })

    $('#CIC').click(function() {
        $('#CICscroll').show();
        $('#DAC5').hide();
        $('#DAC6').hide();
        $('#BSC').hide();
        $('#extraHelp').hide();
    });

    $('#DAC5').click(function() {
        $('#CIC').hide();
        $('#DAC5scroll').show();
        $('#DAC6').hide();
        $('#BSC').hide();
        $('#extraHelp').hide();
    });

    $('#DAC6').click(function() {
        $('#CIC').show();
        $('#DAC5').hide();
        $('#DAC6scroll').show();
        $('#BSC').hide();
        $('#extraHelp').hide();
    });

    $('#BSC').click(function() {
        $('#CIC').show();
        $('#DAC5').hide();
        $('#DAC6').hide();
        $('#BSCscroll').show();
        $('#extraHelp').hide();
    });

    $('#extraHelp').click(function() {
        $('#CIC').show();
        $('#DAC5').hide();
        $('#DAC6').hide();
        $('#BSC').hide();
        $('#extscroll').show();
    });

    $('#CICCircle1').click(function() {
        $('#CIC1Page').show();
    });

    $('#CICCircle2').click(function() {
        $('#CIC2Page').show();
    });

    $('#CICCircle3').click(function() {
        $('#CIC3Page').show();
    });

    $('#CICCircle4').click(function() {
        $('#CIC4Page').show();
    });

    $('#CICCircle5').click(function() {
        $('#CIC5Page').show();
    });

    $('#CICCircle6').click(function() {
        $('#CIC6Page').show();
    });

    $('#CICCircle7').click(function() {
        $('#CIC7Page').show();
    });

    $('#CICCircle8').click(function() {
        $('#CIC8Page').show();
    });

});